{-# LANGUAGE OverloadedStrings #-}
--
module Tezos.Client.Account where
--
import Tezos.Client.Base
--
import Shelly
--
import System.IO
import Control.Monad
--
import Data.Monoid
--
import qualified Data.Maybe as Maybe
import qualified Data.Text as T
--

--
data TZ1 = TZ1
   { tz1Name :: T.Text
   , tz1Addr :: T.Text
   , tz1Key  :: T.Text
   } deriving (Eq)

instance Show TZ1 where
   show (TZ1 n a k) = "tz1::"++(show n)++"<"++(show k)++">"

getHash :: [T.Text] -> Maybe T.Text
getHash ("Hash:" : k : _) = Just k
getHash _ = Nothing

getPublicKey :: [T.Text] -> Maybe T.Text
getPublicKey ("Public" : "Key:" : k : _) = Just k
getPublicKey _ = Nothing

getPrivateKey :: [T.Text] -> Maybe T.Text
getPrivateKey ("Secret" : "Key:" : k : _) = Just k
getPrivateKey _ = Nothing

genTZ1 :: T.Text -> [[T.Text]] -> Maybe TZ1
genTZ1 name xs = do
   let hash = head $ Maybe.catMaybes $ map getHash xs
   let key  = head $ Maybe.catMaybes $ map getPublicKey xs
   return $ TZ1 name hash key
--
getKeys :: T.Text -> Sh T.Text
getKeys name = runTz $ alphaNode <> ["show", "address", name, "-S"]
--
getAllAlias :: Sh [T.Text]
getAllAlias = liftM (Maybe.catMaybes . map (getAccSK . T.words) . T.lines) showKnownAddr
--
showKnownAddr :: Sh T.Text
showKnownAddr = runTz $
   alphaNode <> ["list", "known", "addresses"]
--
getAccSK :: [T.Text] -> Maybe T.Text
getAccSK (name : _ : "(unencrypted" : "sk" : "known)" : _) = Just (T.init name)
getAccSK _ = Nothing
