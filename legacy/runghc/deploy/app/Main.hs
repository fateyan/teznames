{-# LANGUAGE OverloadedStrings #-}
-- {-# LANGUAGE ExtendedDefaultRules #-}
-- {-# OPTIONS_GHC -fno-warn-type-defaults #-}
--
module Main where
--
import Tezos.Client
import Tezos.Data
--
import Shelly
--
import Options.Applicative (execParser, info, idm)
--
import System.IO
import Control.Monad
--
import Data.Monoid
--
import qualified Data.Maybe as Maybe
import qualified Data.Text as T
-- default (T.Text)
--
main = do
   x <- execParser opts
   case x of
      (Misc mopt) -> handleMisc (inFile mopt)
      (ShowKnownTz b) -> show_tz b
      -- (ShowKnownKt) -> show_kt1
--
handleMisc file = do
   s <- readFile file
   let s' = (read s) :: Expr T.Text
   let s'' = {- T.unpack $ -} ppSS s'
   --
   shelly $ print_stdout False $ do
      echo s''

--
-- show_kt = shelly $ print_stdout False $ do
--    d <- run "date" ["+%s"]
--    i <- lastExitCode
--    inspect i
--    inspect "show kt1"
--    --
--    alphaNodeURL <- liftM (Maybe.maybe "node1.lax.tezos.org.sg" id) $ get_env "TEZOS_NODE_URL"
--    inspect alphaNodeURL
--
show_tz b = shelly $ print_stdout False $ do
   ls <- getAllAlias
   let maxNameLength = maximum $ map T.length ls
   xs <- sequence $ map getKeys ls
   showTZ1s b maxNameLength $ Maybe.catMaybes $ map (uncurry genTZ1) $ zip ls $ map (map T.words . T.lines) xs
--





--
spacing :: Bool -> Int -> T.Text -> T.Text
spacing b m name  =
   let nm = T.length name
       foo = if b then flip else id
   in (foo T.append) name $ T.pack (take (m - nm) $ repeat ' ')
--
showTZ1s :: Bool -> Int -> [TZ1] -> Sh ()
showTZ1s b m (x : xs) = do
   echo (showTZ1 b m x)
   showTZ1s b m xs
showTZ1s _ _ [] = return ()
--
showTZ1 :: Bool -> Int -> TZ1 -> T.Text
showTZ1 b m (TZ1 n a k) = T.concat [(spacing b m n)," < ",a," | ",k," >"]
