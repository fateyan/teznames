#!/bin/bash

echo -e "[\033[32mGo in\033[39m] "
cd runghc/deploy
echo -e "[\033[32mCompile\033[39m] "
stack build
echo -e "[\033[32mGet out\033[39m] "
cd ../..
