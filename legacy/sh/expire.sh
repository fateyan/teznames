#!/bin/bash


# dotblack:
#    tz1bhXKVY4ihH8Dcao4cuk8KxJ4sPjXGZcEp
#    edpkuesJWtpYgjM1wjxHn7oiJsT64rMrGXztoa7eQ9MGRCL2GtEm21
# abpS:
#    tz1fjcqKic5pHzFLhUxWBpiSe8KA77jve1TD
#    edpkuecuybjhNpjDwoZKXg3FrrJBmSEz5FsGQBdo7yCjHnfRs1tpDd

PARA1='Right (Pair "edpkuesJWtpYgjM1wjxHn7oiJsT64rMrGXztoa7eQ9MGRCL2GtEm21" (Pair "edsigteVgRT71qbLhEex2QJ4gtEBcyCpj9w19SfkPhiPDxdxjfukxcsQtrg3zLnGmab5qhr5SXJaZ1ty7RHN7nyKatLFuCAdu7b" 0x05010000000a7175657374696f6e3031))'

# PARA2='Right (Pair "edpkuesJWtpYgjM1wjxHn7oiJsT64rMrGXztoa7eQ9MGRCL2GtEm21" (Pair "edsigu3n5Dr4jQNxGmhuHE8YxWzXzUVtme7WxbgiRpxFZGqGYwSTLo3iSVx9nXWu6VArmGPXMj1hKW5iJyw9YPjzQcN7d2sBeSV" 0x05010000000a7175657374696f6e3031))'

contractName=cTzName
origin=alias:dotblack

if [ "$2" != "" ];
then
   tezos-client -A node1.lax.tezos.org.sg transfer 0 from $origin to $contractName --arg "$PARA1" -q --burn-cap $1 $2
else
   tezos-client -A node1.lax.tezos.org.sg transfer 0 from $origin to $contractName --arg "$PARA1" -q --burn-cap $1
fi
