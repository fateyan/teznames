# (I) general info

## GET /v1/isRegistered

Check if the given name is registered

#### Query PARAMETER

```
plaintext_name :: string
```

#### Response (JSON)

200 success

```
registered :: boolean
```

## GET /v1/isPremium

Check if the given name is a premium name

#### Query PARAMETER

```
plaintext_name :: string
```

#### Response (JSON)

200 success

```
registered :: boolean
```

## GET /v1/isRestricted

Check if the given name is a restricted name

#### Query PARAMETER

```
plaintext_name :: string
```

#### Response (JSON)

200 success

```
registered :: boolean
```

## GET /v1/all\_registered\_name

Get the list of all the registered name

#### Response (JSON)

200 success

```
Array [
  tezname :: string
  address :: string
]
```

## GET /v1/restricted\_name\_root\_hash

Get the hash value from the root of the merkle tree of restricted names

#### Response (JSON)

200 success

```
roothash :: string
```

## GET /v1/premium\_name\_root\_hash

Get the hash value from the root of the merkle tree of premium names

#### Response (JSON)

200 success

```
roothash :: string
```

## GET /v1/registration\_fee

Query the fee of registering the given type of name

#### Query PARAMETER

```
nametype :: string -- "reqular" | "premium" | "restricted"
```

#### Response (JSON)

200 success

```
fee :: integer
```

<br/><br/>

# (II) name-based info

## GET /v1/tezname/isExpired

Check if the given name is expired

#### Query PARAMETER

```
plaintext_name :: string
```

#### Response (JSON)

200 success

```
expired :: boolean
```

## GET /v1/tezname/type

Query the type of the given name

#### Query PARAMETER

```
plaintext_name :: string
```

#### Response (JSON)

200 success

```
expired :: string -- "regular" | "premium" | "restricted"
```

## GET /v1/tezname/owner

Query the address of the owner of the given name

#### Query PARAMETER

```
plaintext_name :: string
```

#### Response (JSON)

200 success

```
address :: string
```


## GET /v1/tezname/last\_modification

Query the time and type of the last modification of the given name

#### Query PARAMETER

```
plaintext_name :: string
```

#### Response (JSON)

200 success

```
time :: timestamp
msg  :: string
```


## GET /v1/tezname/timestamps

Query the all time-info of the given name

#### Query PARAMETER

```
plaintext_name :: string
```

#### Response (JSON)

200 success

```
registered_time :: timestamp
expired_time :: timestamp
last_modification_date :: timestamp
```

## GET /v1/tezname/timestamp/register

Query when is the registration time of the given name

#### Query PARAMETER

```
plaintext_name :: string
```

#### Response (JSON)

200 success

```
time :: timestamp
```

## GET /v1/tezname/timestamp/expire

Query when is the expiration time of the given name

#### Query PARAMETER

```
plaintext_name :: string
```

#### Response (JSON)

200 success

```
time :: timestamp
```

## GET /v1/tezname/timestamp/last\_modification

Query when is the last modification time of the given name

#### Query PARAMETER

```
plaintext_name :: string
```

#### Response (JSON)

200 success

```
time :: timestamp
```

<br/><br/>

# (III) registration

## POST /v1/register/regular

Request to register a regular name

#### PARAMETER (JSON)

```
plaintext_name :: string
```

#### Response (JSON)

200 success

```
tezname_address :: string
```

## POST /v1/register/premium

Request to register a premium name

#### PARAMETER (JSON)

```
plaintext_name :: string
merkle_proof :: Array [ string ]
```

#### Response (JSON)

200 success

```
tezname_address :: string
```

## POST /v1/register/restricted

Request to register a restricted name

#### PARAMETER (JSON)

```
plaintext_name :: string
merkle_proof :: Array [ string ]
document :: string -- check sum of legal document
```

#### Response (JSON)

200 success

```
tezname_address :: string
```

## POST /v1/register/subname

Request to register a subname on the given parent name

#### PARAMETER (JSON)

```
root_tezname :: string -- e.g. "john" for "john.tez"
subname :: string -- e.g. "store" for "store.john.tez"
```

#### Response (JSON)

200 success

```
tezname_address :: string
```

## POST /v1/renew

Request to renew the ownership of the given name

#### PARAMETER (JSON)

```
plaintext_name :: string
```

#### Response (JSON)

200 success

```
msg :: string
```
