# The teznames

<img src="img/teznames_logo.jpg" alt="drawing" width="250"/>

The **teznames** is a name service on the [tezos](https://tezos.foundation/) blockchain.

## Users guide

Please read our [Users guide](https://teznames.readthedocs.io/) for more details!

## Contributing

Thanks for your interest in **teznames**. Here are some ways you can get involved and help us improve the teznames!

### contribute documents

The [Users guide](https://teznames.readthedocs.io/) is in branch `docs`. If you're willing to help us to improve our document and users guide, please fork and create a branch for editing. Once you finish you work, please issue an _merge request_ to our `docs` branch. We will review and merge it asap.

### contribute codes

So far the main developing branch is still `master`. If you're willing to help us on improving our source code, please fork and create a branch for editing. Once you finish you work, please issue an _merge request_ to our `master` branch. We will review and merge it asap.

### report bugs

If you found any error or bug, please kindly inform us. All you need is to go to [teznames/issues](https://gitlab.com/tezos-southeast-asia/teznames/issues) page to open a new issue and tag it with the preseted Label: `bug`!

### feedbacks and features

If there is some feature you need or you just want to give some feedback, please go to [teznames/issues](https://gitlab.com/tezos-southeast-asia/teznames/issues) page to open a new issue and tag it with the preseted Label: `discussion` (for feedback) or `suggestion` (for features)!
